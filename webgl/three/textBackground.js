

    // import * as THREE from '../build/three.module.js';

    // import Stats from './jsm/libs/stats.module.js';
    import {OrbitControls} from "./jsm/controls/OrbitControls.js";
    THREE.Cache.enabled = true;

    let container, stats, permalink, hex;
    let camera, cameraTarget, scene, renderer;
    let group, textMesh1, textMesh2, textGeo, materials;
    let firstLetter = true;
    let rotationAllowed = true;
    let text = "Appwrk",
        font = undefined,
        fontName = "optimer", // helvetiker, optimer, gentilis, droid sans, droid serif
        fontWeight = "bold"; // normal bold

    const height = 20,
        size = 90,
        hover = 30;

    

    function init() {
        container = document.createElement( 'div' );
        // document.body.appendChild( container );
        const canvas = document.querySelector('#c');
        var renderer = new THREE.WebGLRenderer({ canvas } );
        // document.body.appendChild( renderer.domElement );
        const fov = 75;
        const aspect = 2;  // the canvas default
        const near = 0.1;
        const far = 100;
        const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
        camera.position.z = 3;

        const controls = new OrbitControls(camera, canvas);
        controls.target.set(0, 0, 0);
        controls.enableZoom = true;
        controls.update();

        const scene = new THREE.Scene();

        {
            const color = 0xFFFFFF;
            const intensity = 1.5;
            const light = new THREE.DirectionalLight(color, intensity);
            light.position.set(-4, 2, 0);
            scene.add(light);
            const amblight = new THREE.AmbientLight(color, 0.75);
            scene.add(amblight);
        }

   
        const loader = new THREE.CubeTextureLoader();
        const texture = loader.load([
            './images/pos-x.jpg',
            './images/neg-x.jpg',
            './images/pos-y.jpg',
            './images/neg-y.jpg',
            './images/pos-z.jpg',
            './images/neg-z.jpg',
        ]);
        scene.background = texture;
        
        const pointLight = new THREE.PointLight( 0xffffff, 1.5 );
        pointLight.position.set( 0, 100, 90 );
        scene.add( pointLight );
       
        pointLight.color.setHSL( 0xffffff, 1, 0.5 );
        hex = '00BDFF';					
        
        materials = [
            new THREE.MeshPhongMaterial( { color: 0xffffff, flatShading: true } ), // front
            new THREE.MeshPhongMaterial( { color: 0xffffff } ) // side
        ];

        group = new THREE.Group();
        group.position.y = 100;

        scene.add( group );
        loadFont();

        const plane = new THREE.Mesh(
            new THREE.PlaneGeometry( 10000, 10000 ),
            // new THREE.MeshBasicMaterial( { color: 0xffffff, opacity: 0.5, transparent: true } )
        );
        plane.position.y = 100;
        plane.rotation.x = - Math.PI / 2;
        scene.add( plane );

        // RENDERER
        // renderer = new THREE.WebGLRenderer( { antialias: true } );
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
        // container.appendChild( renderer.domElement );


        const boxWidth = 0.3;
        const boxHeight = 32;
        const boxDepth = 32;
        // const geometry = new THREE.BoxGeometry(boxWidth, boxHeight, boxDepth);
        const geometry = new THREE.SphereGeometry(boxWidth, boxHeight, boxDepth);

        function makeInstance(geometry, color, x) {
            // const material = new THREE.MeshPhongMaterial({color});
            const material = new THREE.MeshPhongMaterial({color, envMap: texture});
            const cube = new THREE.Mesh(geometry, material);
            scene.add(cube);
            cube.position.x = x;
            return cube;
        }

        const cubes = [
            makeInstance(geometry, 0x44aa88,  0),
            makeInstance(geometry, 0x8844aa, -2),
            makeInstance(geometry, 0xaa8844,  2),
        ];

        // createText();
        function resizeRendererToDisplaySize(renderer) {
            const canvas = renderer.domElement;
            const width = canvas.clientWidth;
            const height = canvas.clientHeight;
            const needResize = canvas.width !== width || canvas.height !== height;
            if (needResize) {
                renderer.setSize(width, height, false);
            }
            return needResize;
        }

        function render(time) {
            time *= 0.001;

            if (resizeRendererToDisplaySize(renderer)) {
                const canvas = renderer.domElement;
                camera.aspect = canvas.clientWidth / canvas.clientHeight;
                camera.updateProjectionMatrix();
            }

            cubes.forEach((cube, ndx) => {
                const speed = 1 + ndx * .1;
                const rot = time * speed;
                cube.rotation.x = rot;
                cube.rotation.y = rot;
            });

            renderer.render(scene, camera);
            requestAnimationFrame(render);
        }

        requestAnimationFrame(render);

        function loadFont() {
            const loader = new THREE.FontLoader();
            loader.load( 'fonts/' + fontName + '_' + fontWeight + '.typeface.json', function ( response ) {
                font = response;
                createText();
            } );
        }

        function createText() {
            textGeo = new THREE.TextGeometry( text, {
                font: font,
                size: size,
                height: height
            } );
            
            textGeo.computeBoundingBox();
            const centerOffset = - 0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );
            textMesh1 = new THREE.Mesh( textGeo, materials );
            textMesh1.position.x = centerOffset;
            textMesh1.position.y = hover;
            textMesh1.position.z = 0;
            textMesh1.rotation.x = 0;
            textMesh1.rotation.y = Math.PI * 2;
            console.log(textMesh1);
            group.add( textMesh1 );				
        }

        // function animate() {
        //     requestAnimationFrame( animate );
        //     render();
        // }

        // function render(time) {				
        //     camera.lookAt( cameraTarget );
        //     // renderer.clear();				
        //     renderer.render( scene, camera );

        //     time *= 0.001;

        //     if (resizeRendererToDisplaySize(renderer)) {
        //         const canvas = renderer.domElement;
        //         camera.aspect = canvas.clientWidth / canvas.clientHeight;
        //         camera.updateProjectionMatrix();
        //     }

        //     cubes.forEach((cube, ndx) => {
        //         const speed = 1 + ndx * .1;
        //         const rot = time * speed;
        //         cube.rotation.x = rot;
        //         cube.rotation.y = rot;
        //     });

        //     requestAnimationFrame(render);
        // }

   
}
init();

