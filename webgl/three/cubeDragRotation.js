// We need 3 things everytime we use Three.js
 // Scene + Camera + Renderer
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 100, window.innerWidth / window.innerHeight, 0.1, 1000 );
const renderer = new THREE.WebGLRenderer({ antialias: true});
 
renderer.setSize( window.innerWidth, window.innerHeight );
 // sets renderer background color
renderer.setClearColor("#222222");
document.body.appendChild( renderer.domElement );
camera.position.z = 5;
 
// resize canvas on resize window
window.addEventListener( 'resize', () => {
   let width = window.innerWidth;
   let height = window.innerHeight;
   renderer.setSize( width, height );
   camera.aspect = width / height;
   camera.updateProjectionMatrix();
});

var materials = [];
for ( var i = 1; i <= 6; i ++ ) {
   if (i < 4) {
      img = i;
   } else {
      img = 'cubetexture';
   }
   materials.push( new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load( './images/'+img+'.png')} ) );
}

// basic cube
// var geometry = new THREE.BoxGeometry( 2, 2, 2, 1);
var geometry = new THREE.CubeGeometry( 2, 2, 2, 1,1,1);
var material = new THREE.MeshStandardMaterial( { color: 0xff0051, flatShading: true, metalness: 0, roughness: 1 });
var material = new THREE.MeshFaceMaterial(materials);
var cube = new THREE.Mesh ( geometry, material );
scene.add( cube );

// ambient light
var ambientLight = new THREE.AmbientLight ( 0xffffff, 0.2);
scene.add( ambientLight );
 
// point light
var pointLight = new THREE.PointLight( 0xffffff, 1 );
pointLight.position.set( 25, 50, 25 );
scene.add( pointLight );
cube.rotation.x += 0.01;
cube.rotation.y += 0.01;
function animate() {
   requestAnimationFrame( animate );
   // cube.rotation.x += 0.04;
   // cube.rotation.y += 0.04;
   // cube.rotation.z += 0.04;
    renderer.render( scene, camera );
}
animate();


var isDragging = false;
var previousMousePosition = {x: 0, y: 0};

$(renderer.domElement).on('mousedown', function(e) {
   isDragging = true;
}).on('mousemove', function(e) {
   // console.log(e);
   var deltaMove = {
      x: e.offsetX-previousMousePosition.x,
      y: e.offsetY-previousMousePosition.y
   };

   if (isDragging) {           
      var deltaRotationQuaternion = new THREE.Quaternion().setFromEuler(new THREE.Euler(toRadians(deltaMove.y * 1), toRadians(deltaMove.x * 1), 0, 'XYZ'));       
      cube.quaternion.multiplyQuaternions(deltaRotationQuaternion, cube.quaternion);
   }   
   previousMousePosition = {x: e.offsetX, y: e.offsetY};
});

function toRadians(angle) {
	return angle * (Math.PI / 180);
}
