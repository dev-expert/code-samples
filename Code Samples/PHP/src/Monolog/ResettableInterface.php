<?php declare(strict_types=1);
  

namespace Monolog;

interface ResettableInterface
{
    /**
     * @return void
     */
    public function reset();
}
