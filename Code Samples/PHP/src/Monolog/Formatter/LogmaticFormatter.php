<?php declare(strict_types=1);
  

namespace Monolog\Formatter;


class LogmaticFormatter extends JsonFormatter
{
    protected const MARKERS = ["sourcecode", "php"];

    /**
     * @var string
     */
    protected $hostname = '';

    /**
     * @var string
     */
    protected $appname = '';

    public function setHostname(string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function setAppname(string $appname): self
    {
        $this->appname = $appname;

        return $this;
    }

    /**
     * Appends the 'hostname' and 'appname' parameter for indexing by Logmatic.
     *
     * @see http://doc.logmatic.io/docs/basics-to-send-data
     * @see \Monolog\Formatter\JsonFormatter::format()
     */
    public function format(array $record): string
    {
        if (!empty($this->hostname)) {
            $record["hostname"] = $this->hostname;
        }
        if (!empty($this->appname)) {
            $record["appname"] = $this->appname;
        }

        $record["@marker"] = static::MARKERS;

        return parent::format($record);
    }
}
