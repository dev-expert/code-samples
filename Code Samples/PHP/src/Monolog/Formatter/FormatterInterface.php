<?php declare(strict_types=1);
  

namespace Monolog\Formatter;


interface FormatterInterface
{
    /**
     * Formats a log record.
     *
     * @param  array $record A record to format
     * @return mixed The formatted record
     *
     * @phpstan-param Record $record
     */
    public function format(array $record);

    /**
     * Formats a set of log records.
     *
     * @param  array $records A set of records to format
     * @return mixed The formatted set of records
     *
     * @phpstan-param Record[] $records
     */
    public function formatBatch(array $records);
}
