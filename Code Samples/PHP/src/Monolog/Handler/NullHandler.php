<?php declare(strict_types=1);
  

namespace Monolog\Handler;

use Monolog\Logger;
use Psr\Log\LogLevel;


class NullHandler extends Handler
{
    /**
     * @var int
     */
    private $level;

    /**
     * @param string|int $level The minimum logging level at which this handler will be triggered
     *
     * @phpstan-param Level|LevelName|LogLevel::* $level
     */
    public function __construct($level = Logger::DEBUG)
    {
        $this->level = Logger::toMonologLevel($level);
    }

    /**
     * {@inheritDoc}
     */
    public function isHandling(array $record): bool
    {
        return $record['level'] >= $this->level;
    }

    /**
     * {@inheritDoc}
     */
    public function handle(array $record): bool
    {
        return $record['level'] >= $this->level;
    }
}
