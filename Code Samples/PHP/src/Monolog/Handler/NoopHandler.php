<?php declare(strict_types=1);
  

namespace Monolog\Handler;


class NoopHandler extends Handler
{
    /**
     * {@inheritDoc}
     */
    public function isHandling(array $record): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function handle(array $record): bool
    {
        return false;
    }
}
