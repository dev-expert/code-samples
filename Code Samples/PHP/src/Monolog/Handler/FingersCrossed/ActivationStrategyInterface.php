<?php declare(strict_types=1);
  

namespace Monolog\Handler\FingersCrossed;


interface ActivationStrategyInterface
{
    /**
     * Returns whether the given record activates the handler.
     *
     * @phpstan-param Record $record
     */
    public function isHandlerActivated(array $record): bool;
}
