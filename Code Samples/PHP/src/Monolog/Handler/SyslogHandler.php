<?php declare(strict_types=1);
  

namespace Monolog\Handler;

use Monolog\Logger;


class SyslogHandler extends AbstractSyslogHandler
{
    /** @var string */
    protected $ident;
    /** @var int */
    protected $logopts;

    /**
     * @param string     $ident
     * @param string|int $facility Either one of the names of the keys in $this->facilities, or a LOG_* facility constant
     * @param int        $logopts  Option flags for the openlog() call, defaults to LOG_PID
     */
    public function __construct(string $ident, $facility = LOG_USER, $level = Logger::DEBUG, bool $bubble = true, int $logopts = LOG_PID)
    {
        parent::__construct($facility, $level, $bubble);

        $this->ident = $ident;
        $this->logopts = $logopts;
    }

    /**
     * {@inheritDoc}
     */
    public function close(): void
    {
        closelog();
    }

    /**
     * {@inheritDoc}
     */
    protected function write(array $record): void
    {
        if (!openlog($this->ident, $this->logopts, $this->facility)) {
            throw new \LogicException('Can\'t open syslog for ident "'.$this->ident.'" and facility "'.$this->facility.'"');
        }
        syslog($this->logLevels[$record['level']], (string) $record['formatted']);
    }
}
