import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {DataItem} from '../../../../common/models/data-item';
import {FormGroup} from '@angular/forms';
import {FieldProps} from '../../../../common/models/field-props';
import {Attachment, AttachmentPropNames} from '../attachment.model';
import * as _ from 'lodash';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../store/store';
import {AuthService} from '../../../../service/auth.service';
import {Subject, Subscription} from 'rxjs';
import {DropdownModel} from '../../../../model/dropdown.model';
import {FormValidationService} from '../../../../service/form-validation.service';
import {Change_Attachment} from '../../store/action';
import {FormsService} from '../../../../service/forms.service';
import {takeUntil} from 'rxjs/operators';
import {AttachmentService} from '../attachment.service';
import {ListItemAction, ListItemActionType} from '../../../../common/models/list-item-action';

@Component({
  selector: 'pat-attachment-new',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit, OnDestroy {
  @Input() contactId: number;
  @Input() vm: DataItem<Attachment> = null;
  @Input() index: number;
  @Output() formChanges = new EventEmitter<{}>(); //this is required for save all feature
  @Output() selectorSaveAll = new EventEmitter<any>();
  @Output() init = new EventEmitter<any>(); //this is required for save all feature
  @Input() attachmentTypes: DropdownModel[] = [];

  public id: string = null;
  public propNames = AttachmentPropNames;
  public form: FormGroup = null;
  public fieldProps: { [key: string]: FieldProps } = null;
  public data: Attachment = null;
  public loggedUser: any = null;
  public idPropToUpdate: string = null;
  public confirmDialog: boolean = false;
  showInActive = false;
  firstActiveRowIndex = 0;
  mainForm: FormGroup;
  attachments: Attachment[] = [];
  loading = false;
  unsubscribe: Subject<any> = new Subject();
  currentDate = new Date();
  isUs: boolean = false;
  private subs: Array<Subscription> = new Array<Subscription>();
  public isActive: boolean;

  constructor(private ngRedux: NgRedux<IAppState>,
              private _authService: AuthService,
              private attachmentService: AttachmentService,
              public formValidation: FormValidationService,
              private formsService: FormsService) {
  }

  ngOnInit() {
    this.isUs = JSON.parse(localStorage.getItem('data')).IsUS;
    this.id = 'pat-attachment-new' + this.index;
    this.loggedUser = this._authService.getLoggedUserInfo();
    this.form = this.vm.metadata.form;
    this.fieldProps = this.vm.metadata.fieldProps;
    this.data = this.vm.data;
    this.initializeFormArray();
    this.init.emit(this);
    this.idPropToUpdate = this.propNames.contact_ID;
    this.updateFormChanges();
  }

  get attachmentForm() {
    return this.attachments;
  }

  initializeFormArray(): void {
    this.mainForm = this.form;
  }

  downloadFile(item: FormGroup): void {
    const link = document.createElement('a');
    link.download = item.controls.name.value;
    link.href = item.controls.path.value;
    link.target = '_blank';
    link.click();
  }

  ngOnDestroy(): void {
    _.forEach(this.subs, (sub: Subscription): void => {
      sub.unsubscribe();
    });
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.ngRedux.dispatch({type: Change_Attachment, todo: this.mainForm});
  }

  setValidation(item: FormGroup): void {
    this.vm.revalidate();
  }

  showValidationMessage(item: FormGroup): boolean {
    return !!item ? !!item.controls[this.propNames.attachmentType_ID].value || !!item.controls[this.propNames.name].value || !!item.controls[this.propNames.endDate].value || !!item.controls[this.propNames.startDate].value || !!item.controls[this.propNames.note].value : false;
  }

  public openDeleteModal() {
    this.confirmDialog = true;
  }

  closeModal(): void {
    this.confirmDialog = false;
  }

  archiveRow(): void {
    /* this code is used to set null value after delete the record */
    if (this.form.controls[this.propNames.attachment_ID].value > 0) {
      this.form.controls[this.propNames.isActive].setValue(false);
      this.form.controls[this.propNames.isArchived].setValue(true);
      this.formsService.clearErrors(this.form);
    }
    this.form.markAsDirty({onlySelf: true});
    this.vm.metadata.visible = false;
    this.attachmentService.postAction(new ListItemAction<Attachment>(
      {
        action: ListItemActionType.remove,
        index: this.index,
        dataItem: this.vm
      }));
    this.confirmDialog = false;
  }

  updateFormChanges(): void {
    this.form.valueChanges.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      if (this.form.dirty) {
        this.formChanges.emit();
      }
    });
  }
}
