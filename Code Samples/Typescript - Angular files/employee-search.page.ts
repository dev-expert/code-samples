import { Component, EventEmitter, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { ContactSearchCriteria } from '../module/contact/model/contact-search-criteria.model';
import { Subscription } from 'rxjs/Subscription';
import { Common } from '../common/common';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CommonHttpService } from 'src/app/service/common-http.service';
import { SearchEmployee } from '../model/searchEmployee.model';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { BehaviourSubjectService } from 'src/app//service/behaviour-subject.service';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../store/store';
import { Change_EmployeeSearch } from '../store/action';
import { CommonService } from 'src/app/service/common.service';
import { ContactType } from '../module/contact/enums/contact-type.enum';
import { CountryStateService } from '../service/country-state.service';
import { Country } from '../module/contact/model/country.model';

@Component({
    selector: 'employee-search',
    templateUrl: './employee-search.page.html',
})
export class EmployeeSearchComponent implements OnInit, OnDestroy {
    searchCriteria = new ContactSearchCriteria();
    searchSubscriber: Subscription;
    loader = false;
    contactAutoComplete: any;
    contactEmit = new EventEmitter<any>();
    unsubscribe: Subject<any> = new Subject();
    searchResults: SearchEmployee[];
    lastSearchTerm: string;
    showLeftOptions = false;
    authrisedUserName: string = '';
    loading = false;
    country: Country;
    count = 0;
    employeeSearchResult: any;
    constructor(private authService: AuthService, private httpService: CommonHttpService, public countryStateService: CountryStateService, private router: Router, private behaviourService: BehaviourSubjectService,
        private ngRedux: NgRedux<IAppState>, private renderer: Renderer2, public commonService: CommonService) {
        this.contactEmit.pipe(debounceTime(Common.mainSearchDefaultDebounceTime)).pipe(takeUntil(this.unsubscribe)).subscribe(obj => this.filter());
        const userInfo = this.authService.getLoggedUserInfo();
        this.searchCriteria.SecurityGroup_ID = userInfo.SecurityGroup_ID;
        this.country = new Country();
        this.countryStateService.getCountries();
    }
    ngOnInit() {
        this.renderer.addClass(document.body, 'pat-employee-search-view');
        if (this.authService.userInfo) {
            this.authrisedUserName = this.authService.userInfo.profile.given_name;
        }

        const employeeSearchStore = this.ngRedux.getState();
        if (!!employeeSearchStore.employeeSearch) {
            if (employeeSearchStore.employeeSearch['employeeSearchCriteria']) {
                this.searchCriteria = employeeSearchStore.employeeSearch['employeeSearchCriteria'];
            }
            if (employeeSearchStore.employeeSearch['employeeSearchResults']) {
                this.searchResults = employeeSearchStore.employeeSearch['employeeSearchResults'];
            }
        }
        this.behaviourService.employeeAdvancedSearch.subscribe((data: any) => {
            if (data && data.contactType == ContactType.Individual)
                this.filter(data);
        })
        if (!!sessionStorage.getItem('employeeAdvancedSearch')) {
            this.employeeSearchResult = JSON.parse(sessionStorage.getItem('employeeAdvancedSearch'));
            if (this.employeeSearchResult && this.employeeSearchResult.contactType == ContactType.Individual)
                this.filter(this.employeeSearchResult);
        }
    }

    filterKeyup(contactAutoComplete) {
        if (this.searchCriteria.SearchValue && this.searchCriteria.SearchValue.length >= 3) {
            if (this.searchSubscriber) {
                this.loader = false;
                this.searchSubscriber.unsubscribe();
            }
            contactAutoComplete['loading'] = true;
            this.contactAutoComplete = contactAutoComplete;
            this.contactEmit.emit(name);
        }
    }

    filter(data = null) {
        this.searchResults = [];
        if (!data) {
            if (this.searchCriteria.SearchValue && this.lastSearchTerm === this.searchCriteria.SearchValue.trim()) {
                return;
            }
            this.searchCriteria.ContactCategory_IDs = '15';
            this.searchCriteria.ContactType = ContactType.Individual;
            this.searchCriteria.ResultsView = 3;
        } else {
            this.searchCriteria.ResultsView = 3;
            this.searchCriteria.SearchValue = data.searchValue;
            this.searchCriteria.ContactCategory_IDs = data.contactCategory_IDs;
            this.searchCriteria.EmployeeRole_IDs = data.employeeRole_IDs;
            this.searchCriteria.ContactType = data.contactType;
            this.searchCriteria.Tribe_ID = data.tribe_Id;
            this.searchCriteria.Pod_IDs = data.pod_IDs;
            this.searchCriteria.AgencyContact_ID = data.agencyContact_ID;
            this.searchCriteria.IsParadigmAgency = data.isParadigmAgency;
        }
        this.loading = true;
        this.searchSubscriber = this.httpService.post<Array<SearchEmployee>>(`search/employeeList`, this.searchCriteria).subscribe(res => {
            this.loading = false;
            this.searchResults = res;
            if (!!sessionStorage.getItem('employeeAdvancedSearch'))
                sessionStorage.removeItem('employeeAdvancedSearch');
            this.behaviourService.advancedEmployeeSearch(null);
            this.lastSearchTerm = this.searchCriteria.SearchValue;
            if (this.searchResults.length > 0) {
                for (var i = 0; i <= this.searchResults.length - 1; i++) {
                    let directPhone = this.searchResults[i]["directPhoneNumbers"];
                    let businessPhone = this.searchResults[i]["businessPhoneNumbers"];
                    let mobilePhone = this.searchResults[i]["mobilePhoneNumbers"];
                    if (directPhone.length > 0) {
                        for (var k = 0; k <= directPhone.length - 1; k++) {
                            if (this.isMaskedCountry(directPhone[k])) {
                                directPhone[k].number = this.commonService.formatPhoneNumber(directPhone[k].number);
                            }
                        }
                    }
                    if (businessPhone.length > 0) {
                        for (var k = 0; k <= businessPhone.length - 1; k++) {
                            if (this.isMaskedCountry(businessPhone[k])) {
                                businessPhone[k].number = this.commonService.formatPhoneNumber(businessPhone[k].number);
                            }
                        }
                    }
                    if (mobilePhone.length > 0) {
                        for (var k = 0; k <= mobilePhone.length - 1; k++) {
                            if (this.isMaskedCountry(mobilePhone[k])) {
                                mobilePhone[k].number = this.commonService.formatPhoneNumber(mobilePhone[k].number);
                            }
                        }
                    }
                }
            }
        }, err => {
            this.loader = false;
            if (err.status === 401 && this.count <= Common.defaultAPICallCount) {
                this.filter();
                this.count = this.count + 1;
            }
            throw err;
        });
    }

    isMaskedCountry(phoneNumber: any) {
        this.country.country_ID = phoneNumber.country_ID;
        return this.countryStateService.isMaskedCountry(this.country);
    }

    detail(item) {
        localStorage.setItem('isFromEmployeeSearch', 'true');
        this.router.navigate(['/contact/edit/' + item.contact_ID]);
    }
    ngOnDestroy() {
        const employeeSearch = {
            employeeSearchCriteria: this.searchCriteria,
            employeeSearchResults: this.searchResults,
            // mySelection: this.mySelection
        };
        this.unsubscribe.next();
        this.unsubscribe.complete();
        this.ngRedux.dispatch({ type: Change_EmployeeSearch, todo: employeeSearch });
        this.renderer.removeClass(document.body, 'pat-employee-search-view');
    }
}