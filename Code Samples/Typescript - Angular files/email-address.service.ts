import {Injectable} from '@angular/core';
import {CommonHttpService} from '../../../service/common-http.service';
import {Observable, BehaviorSubject, Subject, Subscription} from 'rxjs';
import {EmailAddress} from './email-address.model';
import {DropdownModel} from '../../../model/dropdown.model';
import _ from 'lodash';
import {takeWhile} from 'rxjs/operators';
import {ListItemAction} from '../../../common/models/list-item-action';
import {ISectionConfigSlice, SectionState} from '../section-menu/state/section-config.slice';
import {DataItem} from '../../../common/models/data-item';
import {FormsService} from '../../../service/forms.service';
import {ToastrService} from 'ngx-toastr';
import {select, Store} from '@ngrx/store';
import {getSaveAllForSection, RemoveSaveAllForSectionAction, SetSectionStateAction} from '../section-menu/state';
import {FormGroup} from '@angular/forms';
import {getEmailAddress, IEmailAddressSlice} from './state';

@Injectable({
  providedIn: 'root'
})
export class EmailAddressService {

  private emailAddressTypeSubject = new BehaviorSubject<Array<DropdownModel>>(null);
  public emailAddressTypeSubject$ = this.emailAddressTypeSubject.asObservable();
  private emailAddressSubject = new Subject<ListItemAction<EmailAddress>>();
  public emailAddressSubject$ = this.emailAddressSubject.asObservable();
  private baseControllerRoute: string = 'EmailAddress';
  private emailAddressTypes: Array<DropdownModel> = null;
  private saveEmailAddressesSubject = new Subject<Array<EmailAddress>>();
  public saveEmailAddressesSubject$ = this.saveEmailAddressesSubject.asObservable();

  public selectorName: string = 'pat-email-address-list';
  public loading: boolean = false;

  private _componentInView: boolean = null;
  private _emailAddresses: Array<DataItem<EmailAddress>> = null;
  private getEmailAddressesSub: Subscription = null;
  private getSaveAllForSectionSub: Subscription = null;
  private sectionState = new SectionState(this.selectorName);

  public get componentInView() {
    return this._componentInView;
  }

  public set componentInView(inView: boolean) {
    this._componentInView = inView;
    if (this._componentInView === true) {
      this._emailAddresses = null;
      if (!_.isNull(this.getEmailAddressesSub)) {
        this.getEmailAddressesSub.unsubscribe();
      }
      if (!_.isNull(this.getSaveAllForSectionSub)) {
        this.getSaveAllForSectionSub.unsubscribe();
      }
    } else if (this._componentInView === false) {
      this.getEmailAddresses();
      this.getSaveAllForSection();
    }
  }

  constructor(private httpService: CommonHttpService,
              private formsService: FormsService,
              private sectionConfigStore: Store<ISectionConfigSlice>,
              private addressStore: Store<IEmailAddressSlice>,
              private toastr: ToastrService) {
    this.getEmailAddressTypes();
  }

  public get(contactId: number): Observable<Array<EmailAddress>> {
    return this.httpService.get<Array<EmailAddress>>(`${this.baseControllerRoute}/${contactId}`);
  }

  public save(emailAddresses: Array<EmailAddress>): Observable<Array<EmailAddress>> {
    return this.httpService.post<Array<EmailAddress>>(`${this.baseControllerRoute}`, emailAddresses);
  }

  public get EmailAddressSectionMenus(): Array<DropdownModel> {
    return _.cloneDeep(this.emailAddressTypes);
  }

  public postAction(itemAction: ListItemAction<EmailAddress>) {
    this.emailAddressSubject.next(itemAction);
  }

  public getEmailAddressTypes(): Observable<Array<DropdownModel>> {
    if (!_.isNull(this.emailAddressTypes)) {
      this.emailAddressTypeSubject.next(this.emailAddressTypes);
      return;
    }
    this.httpService.get<Array<DropdownModel>>(`${'Lookup/EmailAddressType'}`)
      .subscribe((resp: Array<DropdownModel>) => {
        this.emailAddressTypes = resp;
        this.emailAddressTypeSubject.next(this.emailAddressTypes);
      });
  }

  private getEmailAddresses(): void {
    this.getEmailAddressesSub = this.addressStore.pipe(
      select(getEmailAddress),
      takeWhile(() => this._componentInView === false))
      .subscribe((vm: Array<DataItem<EmailAddress>>) => {
        this._emailAddresses = vm;
      });
  }

  private getSaveAllForSection(): void {
    this.getSaveAllForSectionSub = this.sectionConfigStore.pipe(
      select(getSaveAllForSection, this.selectorName),
      takeWhile(() => this._componentInView === false))
      .subscribe((saveAll: boolean) => {
        if (saveAll === true) {
          this.saveAddresses(this._emailAddresses);
        }
      });
  }

  public saveAddresses(vm: Array<DataItem<EmailAddress>>): void {
    const formGroups: Array<FormGroup> = this.formsService.getFormGroupsFromDataItems<EmailAddress>(vm);
    const isPristine: boolean = this.formsService.isFormPristine(formGroups);
    if (isPristine === true) {
      return;
    }
    const formGroupsToSave = this.formsService.getFormGroupsToSave(formGroups);
    const addressesToSave: Array<EmailAddress> = _.map(formGroupsToSave, (formGroup: FormGroup) => {
      return new EmailAddress(formGroup.value);
    });
    this.loading = true;
    const sub = this.save(addressesToSave)
      .pipe()
      .subscribe((response: Array<EmailAddress>): void => {
        this.toastr.success('Saved successfully!');
        this.sectionConfigStore.dispatch(new RemoveSaveAllForSectionAction(this.selectorName));
        this.saveEmailAddressesSubject.next(response);
        if (this.componentInView === false) {
          this.updateSectionMenuInStore([]);
        }
        this.loading = false;
        sub.unsubscribe();
      }, (err) => {
        sub.unsubscribe();
        this.loading = false;
        this.toastr.error(err.error.error);
      });
  }

  public updateSectionMenuInStore(vm: Array<DataItem<EmailAddress>>): void {
    const formGroups = this.formsService.getFormGroupsFromDataItems<EmailAddress>(vm);
    const newPristine = this.formsService.isFormPristine(formGroups);
    const newValid = this.formsService.isFormValid(formGroups);
    if (newPristine !== this.sectionState.pristine || newValid !== this.sectionState.valid) {
      this.sectionState.pristine = newPristine;
      this.sectionState.valid = newValid;
      this.sectionConfigStore.dispatch(new SetSectionStateAction(this.sectionState));
    }
  }
}
