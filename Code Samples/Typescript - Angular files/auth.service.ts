/// <reference path='../../../node_modules/@types/adal/index.d.ts' />
import {Injectable} from '@angular/core';
import 'expose-loader?AuthenticationContext!../../../node_modules/adal-angular/lib/adal.js';
import {TempoConfig} from '../common/models/tempo.config';
import Config = adal.Config;

const createAuthContextFn: adal.AuthenticationContextStatic = AuthenticationContext;

@Injectable()

export class AuthService {

  private context: adal.AuthenticationContext;
  private adalConfig: Config = {
    tenant: TempoConfig.props.adConfig.tenant,
    clientId: TempoConfig.props.adConfig.clientId,
    redirectUri: TempoConfig.props.adConfig.redirectUri,
    postLogoutRedirectUri: TempoConfig.props.adConfig.redirectUri,
    extraQueryParameter: TempoConfig.props.adConfig.extraQueryParameter,
    expireOffsetSeconds: 3000,
    cacheLocation: 'localStorage',
    isAngular: true
  };

  constructor() {
    this.context = new createAuthContextFn(this.adalConfig);
  }

  login() {
    localStorage.clear();
    this.context.login();
  }

  logout() {
    localStorage.clear();
    this.context.logOut();
  }

  handleCallback() {
    console.log('handle callback works!');
    this.context.handleWindowCallback();
  }

  public acquireToken(): Promise<any> {
    return new Promise(resolve => {
      const user = this.context.getCachedUser();
      if (user) {
        this.context.acquireToken(this.adalConfig.clientId, function (errorDesc, token) {
          if (token) {
            resolve(true);
          } else {
            resolve(false);
          }

        }.bind(this));
      } else {
        this.context.login();
      }
    });
  }

  public refreshToken(): Promise<any> {
    return new Promise(resolve => {
      const user = this.context.getCachedUser();
      if (user) {
        this.context.acquireToken(this.adalConfig.clientId, function (errorDesc, token) {
          this.handleCallback();
          resolve(token);
        }.bind(this));
      }
    });
  }

  public getUserAccessToken() {
    return this.getCachedToken();
  }

  public getCachedToken(resource?) {
    if (resource) {
      return this.context.getCachedToken(resource);
    }
    const token = this.context.getCachedToken(this.adalConfig.clientId);
    return token ? token : localStorage.getItem('adal.idtoken');
  }

  public get userInfo() {
    return this.context.getCachedUser();
  }

  public get getCachedUser() {
    return this.context.getCachedUser();
  }

  public get isAuthenticated() {
    const token = this.getCachedToken();
    return this.userInfo && token;
  }

  //custom function for storing user information service variable
  public setUserInfo(info) {
    if (!localStorage.getItem('data')) {
      localStorage.setItem('data', JSON.stringify(info));
    }
  }

  public getLoggedUserInfo() {
    if (!!localStorage.getItem('hypernatedUser')) {
      return JSON.parse(localStorage.getItem('hypernatedUser'));
    }
    return JSON.parse(localStorage.getItem('data'));
  }
}
