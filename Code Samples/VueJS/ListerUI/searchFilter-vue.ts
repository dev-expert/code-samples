import { findIndex } from 'lodash'
import { mapActions } from 'vuex'
declare var $: any;

export default {
  // https://vuejs.org/v2/guide/components.html#Prop-Validation
  data: function () {
    return {
      attributes: [],
      id: '',
      value: '',
      operator: '',
      attribute_select: '',
      saved_search_select: '',
      operators: [],
      search_filters: [],
      saved_search: {
        title: ''
      },
      saved_searches: []
    }
  },
  mounted() {
    $('.daterange-single').daterangepicker({
      singleDatePicker: true
    });
    this.fetchSavedSearches();
    this.fetchAttributes();
  },
  computed: {
    disableSaveButton() {
      if (this.saved_searches.length) {
        var index = findIndex(this.saved_searches, { 'title': 'Select a Saved Filter' })
        if (index >= 0)
          return (this.saved_search_select != this.saved_searches[index].id || !this.search_filters.length)
      }
      return true
    },
    disableAddButton() {
      if (this.saved_searches.length) {
        var index = findIndex(this.saved_searches, { 'title': 'Select a Saved Filter' })
        if (index >= 0)
          return this.saved_search_select != this.saved_searches[index].id
      }
    },
  },
  methods: {
    ...mapActions(['setMessage', 'blockUi', 'setMessage', 'saveSavedSearchFilter', 'saveAddSearchFilter', 'updateFilter', 'getSearchFilter', 'deleteSearchFilter', 'deleteSavedSearch', 'getSavedSearches', 'getFetchAttributes', 'getFetchOperatorsByAttribute']),
    fetchAttributes() {
      this.getFetchAttributes().then((response) => {
        if (response.status === 200) {
          this.attributes = response.data.data.attributes
          // this.attribute_select = Object.keys(this.attributes)[0]
        }
      })
    },
    attributeSelect2() {
      $("#attributes_select").select2(
      ).on(
        'change',
        (e) => {
          this.attribute_select = $(e.target).val();
          if (this.attribute_select)
            this.fetchOperators(this.attribute_select)
        }
        );
    },
    editFilter(sf) {
      this.id = sf.id;
      this.value = sf.value;
      this.operator = sf.operator;
      var keys = Object.keys(this.attributes);
      for (var i = 0; i < keys.length; i++) {
        if (keys[i].indexOf(sf.attribute_name) >= 0) {
          this.attribute_select = keys[i];
          this.attributeSelect2();
          $("#attributes_select").val(this.attribute_select).trigger("change");
          $("#addFilter_modal").modal('show');
          break;
        }
      }
    },

    fetchOperators(name) {
      this.getFetchOperatorsByAttribute({ name }).then((response) => {
        if (response.status === 200) {
          this.operators = response.data.data.operators
          if (!this.operator)
            this.operator = this.operators[0];
        }
      })
    },
    updateSearchFilter() {
      this.blockUi("addFilterForm");
      var searchFilter = {
        'operator': this.operator,
        'value': this.value
      }
      this.updateFilter({ id: this.id, search_filter: searchFilter }).then((response) => {
        if (response.status === 200) {
          $("#addFilter_modal").modal('hide');
          this.fetchSearchFilters(true);
          this.$emit('refresh', { filterValue: this.saved_search_select, pageReset: true })
        }
      })
    },
    addSearchFilter(name) {
      if (this.id) {
        this.updateSearchFilter();
        return
      }
      this.blockUi("addFilterForm");
      var searchFilter = {
        'id': this.id,
        'attribute_name': this.attribute_select,
        'operator': this.operator,
        'value': this.value
      }
      this.saveAddSearchFilter({ search_filter: searchFilter }).then((response) => {
        if (response.status === 200) {
          $("#addFilter_modal").modal('hide');
          this.search_filters.push(response.data.search_filter)
          this.fetchSavedSearches()
          this.$emit('refresh', { filterValue: this.saved_search_select, pageReset: true })
        }
      })
    },

    fetchSearchFilters(pageReset) {
      this.$emit('refresh', { filterValue: this.saved_search_select, pageReset: pageReset })
      this.getSearchFilter(this.saved_search_select).then((response) => {
        if (response.status === 200) {
          this.search_filters = response.data.search_filters
        }
      })
    },

    removeSearchFilter(id, index) {
      this.deleteSearchFilter({ id: id, saved_search_id: this.saved_search_select }).then((response) => {
        if (response.status === 200) {
          this.search_filters.splice(index, 1)
          this.$emit('refresh', { filterValue: this.saved_search_select, pageReset: false })
          if (this.search_filters.length <= 0) this.fetchSavedSearches()
        }
      })
    },

    saveSearch() {
      this.blockUi("saveFilterForm");
      this.saveSavedSearchFilter({ saved_search: { title: this.saved_search.title } }).then((response) => {
        if (response.status === 200) {
          var index = findIndex(this.saved_searches, { 'id': response.data.saved_search.id })
          if (index === -1) this.saved_searches.push(response.data.saved_search)
          this.setMessage({ type: 'Success', message: 'Filter saved.' })
          $("#saveFilter_modal").modal('hide');
          this.saved_search_select = this.saved_searches[this.saved_searches.length - 1].id
        }
      })
    },

    fetchSavedSearches() {
      var _current = this;
      this.getSavedSearches().then((response) => {
        if (response.status === 200) {
          _current.saved_searches = response.data.saved_searches;
          var index = findIndex(_current.saved_searches, { 'title': 'Current' })
          if (index >= 0)
            _current.saved_searches[index].title = "Select a Saved Filter";
          $("#savedSearches").select2(
          ).on(
            'change',
            (e) => {
              _current.saved_search_select = $(e.target).val();
              _current.fetchSearchFilters(true);
            }
            );
          this.setCurrentSavedSearch()
        }
      })
    },

    removeSavedSearch() {
      this.deleteSavedSearch(this.saved_search_select).then((response) => {
        if (response.status === 200) {
          var index = findIndex(this.saved_searches, { 'id': this.saved_search_select })
          this.saved_searches.splice(index, 1)
          this.setMessage({ type: 'Success', message: 'Filter removed.' })
          this.setCurrentSavedSearch()
        }
      })
    },

    setCurrentSavedSearch() {
      var index = findIndex(this.saved_searches, { 'title': 'Select a Saved Filter' })
      if (index !== -1) {
        this.saved_search_select = this.saved_searches[index].id
        this.fetchSearchFilters(false)
      }
    },
    addFilter() {
      this.value = '';
      this.operator = '';
      this.attribute_select = '';
      this.id = '';
      this.attributeSelect2();
      $("#addFilter_modal").modal('show');
    },
    saveFilter() {
      this.saved_search.title = '';
      $("#saveFilter_modal").modal('show');
    }
  }
}
