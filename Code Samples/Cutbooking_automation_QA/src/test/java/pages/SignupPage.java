package pages;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Email.VerificationLink;

import java.io.IOException;
import java.util.ArrayList;

import static org.testng.AssertJUnit.assertEquals;
import static utils.extentreports.ExtentTestManager.getTest;

public class SignupPage extends BasePage {
    commonfunction cf = new commonfunction(driver);
    By EmailAddress = By.name("email");
    By Term_Agree_Checkbox = By.xpath("//span[@class='checkbox-tick']");
    By Required_filed_alert = By.xpath("//div[contains(text(),'This field is required')]");
    By password_button = By.name("password");
    By Submit_button = By.xpath("(//button[normalize-space()='Sign Up'])[1]");
    By Continue_button = By.xpath("//button[normalize-space()='Continue']");
    By first_name = By.name("first_name");
    By last_name = By.name("last_name");
    By tel = By.name("tel");
    By submit = By.xpath("//button[@type='submit']");

    public SignupPage(WebDriver driver) {
        super(driver);
    }

    public ArrayList<String> Open_login_Page(String url) throws Exception {
        ArrayList<String> steps = new ArrayList<String>();

        driver.get(url);
        steps.add(writelog("Load Site Url " + url));
        return steps;
    }
    public ArrayList<String> Click_SignUp_Page(String username, String password) throws Exception {
        ArrayList<String> steps = new ArrayList<String>();
        getTest().info(MarkupHelper.createLabel("Sign-Up page", ExtentColor.BLUE));
        Action_Write(EmailAddress, username);
        steps.add(writelog("Input Email Address"+username));
        Action_click(Continue_button);
        steps.add(writelog("Press Continue button"));
        Action_clear(password_button);
        steps.add(writelog("Cleared the password field"));
        Action_Write(password_button, password);
        steps.add(writelog("Input Password "+password));

        ScrollToElement(Term_Agree_Checkbox);
        steps.add(writelog("Scrolled to the term and condition Checkbox position"));
        JavascriptCLick(Term_Agree_Checkbox);
        steps.add(writelog("Click on Term and condition Checkbox"));
        JavascriptCLick(Submit_button);

        Thread.sleep(5000);
        steps.add(writelog("Pressed Submit Button"));
        SignupPage sp = new SignupPage(driver);
        return steps;
    }

    public ArrayList verifyEmail() throws Exception {
        ArrayList<String> steps = new ArrayList<String>();
        VerificationLink vfl = new VerificationLink();
        String url = VerificationLink.Verificaion("verify","Verify Email Address").replace("&amp;", "&");
        steps.add(writelog("Take Verification Link from EMail APi "+url));
        driver.get(url);
        steps.add(writelog("Opened Verification Link"));
        return steps;
    }

    public ArrayList<String> Your_details_Info() throws Exception {
        ArrayList<String> steps = new ArrayList<String>();
        steps.add(writelog("Input Your Details Info"));
        Action_click(first_name);
        steps.add(writelog("Click on First Name field"));
        Action_Write(first_name, "Zakaria");
        steps.add(writelog("Write  First Name Zakaria"));
        Action_Write(last_name, "Shahed");
        steps.add(writelog("Write Last name Shahed"));
        Action_Write(tel, "+14xxxxxxxx");
        steps.add(writelog("Input Telephone Number +14xxxxxxxx"));
        Action_click(submit);
        steps.add(writelog("Click On submit Button"));
        return steps;
    }


    public ArrayList<String> property_details(String text) throws Exception {
        ArrayList<String> steps = new ArrayList<String>();
        if (text.equals("WithoutProperty")) {
            steps.add(writelog("Click on Checkbox I don't have a property, or want to register it later"));
            By radioCheckbox = By.xpath("//label[normalize-space()='I have a property and want to register it now']/preceding::label[3]");
            Action_click(radioCheckbox);
            JavascriptCLick(submit);
            assertEquals(driver.getTitle(), "CultBooking - Properties");
            steps.add(writelog("Press Submit button after input property details its go to Properties page"));

        }
        if (text.equals("withproperty")) {
            steps.add(writelog("Click on Checkbox I have a property and want to register it now"));
            By radioCheckbox = By.xpath("//label[normalize-space()='I have a property and want to register it now']/preceding::label[1]");
            Action_click(radioCheckbox);
        }


        Thread.sleep(3000);
        return steps;
    }
    By NameOfTheProperty = By.xpath("//input[@name='name']/ancestor::fieldset[1]");
    By Street = By.name("street");
    By Phone = By.name("htel");
    By zip = By.name("zip");
    By city = By.name("city");
    By country = By.xpath("//div[@class='ss-single-selected']");
    By Write_Country_Name = By.xpath("//input[@placeholder='Search']");
    By SelectCountry = By.xpath("//div[@class='ss-option']");
    By email = By.name("email");
    public ArrayList<String> I_have_a_property_and_want_to_register_it_now() throws Exception {
        ArrayList<String> steps = new ArrayList<String>();

        Action_Write(NameOfTheProperty, "Hotel Rupdia");
        steps.add(writelog("Add Hotel Name"));
        Action_Write(Street, "House no 12,Road no 12, Sector 10, Uttara Model Town");
        steps.add(writelog("Add Address"));
        Action_Write(Phone, "+18888888888");
        steps.add(writelog("Add Phone Number"));
        Action_Write(zip, "1230");
        steps.add(writelog("Add zip Code"));
        Action_Write(city, "Jashore");
        steps.add(writelog("Add City Info"));
        ScrollToElement(submit);
        steps.add(writelog("Scroll down"));
        Action_click(country);
        writeText_Enter(Write_Country_Name, "Bangladesh");
        steps.add(writelog("Select a country"));
        click(SelectCountry);
        steps.add(writelog("Write Email Address : admin@propertyowner.com"));
        Action_Write(email, "admin@propertyowner.com");

        Action_click(submit);
        Thread.sleep(10000);
        steps.add(writelog("Press Submit Button"));
        assertEquals(driver.getTitle(), "CultBooking - Setup");
        return steps;
    }
    By Systems=By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Systems'])[1]/following::span[2]");
    By Property=By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Property'])[1]/following::span[2]");
    By Submit=By.xpath("//button[@type='submit']");

    public void PrintHotelName(String url) throws Exception {
        driver.get(url);
        ArrayList<String> steps = new ArrayList<String>();
        steps.add(writelog("Input email address : xyz@gmail.com"));
        By username=By.name("email");
        Action_Write(username,"xyz@gmail.com");
        By password=By.name("password");
        steps.add(writelog("Input password : xxxxxx"));
        Action_Write(password,"xxxxxx");
        steps.add(writelog("Very Submit"));
        Action_click(Submit);
        Thread.sleep(15000);
        Action_click(Systems);
        Action_click(Property);
        Thread.sleep(15000);
        String val = driver.findElement(By.xpath("//div[@class='bv-no-focus-ring']//input[@name='name']")).getAttribute("value");
        System.out.println(val);

    }

}