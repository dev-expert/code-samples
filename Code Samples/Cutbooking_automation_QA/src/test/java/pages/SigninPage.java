package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.IOException;
import java.util.ArrayList;

public class SigninPage extends BasePage{

    public SigninPage(WebDriver driver) {
        super(driver);
    }

    public ArrayList<String> Signin(String username, String password) throws Exception {
        ArrayList<String> steps = new ArrayList<String>();
        SignupPage sp=new SignupPage(driver);
        Action_click(sp.EmailAddress);
        Action_Write(sp.EmailAddress,username);
        steps.add(writelog("Input Email Address"+username));
        Action_Write(sp.password_button,password);
        steps.add(writelog("Input Password "+password));
        Action_click(sp.submit);
        steps.add(writelog("Clicked on the submit button"));
return steps;
    }
    public ArrayList<String> SigninVerification() throws Exception {
        Thread.sleep(10000);
        ArrayList<String> steps = new ArrayList<String>();
        System.out.println(driver.getTitle());
        steps.add(writelog("Verified login sucessfully in the dashboard"));
        Assert.assertTrue(driver.getTitle().contains("CultBooking - Setup")||driver.getTitle().contains("CultBooking - Dashboard")||driver.getTitle().contains("CultBooking - Properties"));
        return steps;
    }
}
