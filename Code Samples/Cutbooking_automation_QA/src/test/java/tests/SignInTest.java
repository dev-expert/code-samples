package tests;

import Configuration.SiteConfig;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.SigninPage;
import pages.SignupPage;
import pages.commonfunction;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static utils.extentreports.ExtentTestManager.startTest;

public class SignInTest extends BaseTest{

    @DataProvider(name = "SigninLink")
    public Object[][] getData() {
        Object[][] data = SiteConfig.SiteUrlForSignin;
        return data;
    }


    @Test(priority = 1, alwaysRun = true, dataProvider = "SigninLink", description = "Signup from landing page (creating new accounts) and select I don't have a property or want to register it later ")
    public void TC005_Extranet_Signin_to_existing_accounts(String url,String username,String password,Method method,ITestContext ctx) throws Exception {
        startTest(method.getName()+" for User name "+username, "SignIn  account for the sites "+url+" for User name "+username);
        ctx.getCurrentXmlTest().addParameter("title", "Signin for the url" + url +" User name "+username+" and password "+password);
        ArrayList<String> all_log = new ArrayList<String>();
        SignupPage sp = new SignupPage(driver);
        SigninPage si = new SigninPage(driver);
        commonfunction cf = new commonfunction(driver);
        try {
            all_log.addAll(sp.Open_login_Page(url));
            all_log.addAll(si.Signin(username,password));
            all_log.addAll(si.SigninVerification());
        } catch (Throwable e) {
            ctx.getCurrentXmlTest().addParameter("description", cf.Steps_for_Gitlab_reporting(all_log));
            throw new Exception(e);
        }

    }

}
