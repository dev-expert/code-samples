package tests;

import Configuration.SiteConfig;
import io.qameta.allure.*;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.SignupPage;
import pages.commonfunction;
import utils.listeners.TestListener;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static utils.extentreports.ExtentTestManager.startTest;

@Listeners({TestListener.class})
@Epic("Regression Tests")
@Feature("Login Tests")
public class SignupTest extends BaseTest {

    @DataProvider(name = "SignupLink")
    public Object[][] dpMethod() {
        return SiteConfig.SiteUrlForSignUp;
    }

    @Test(priority = 0, alwaysRun = true, dataProvider = "SignupLink", description = "Signup from landing page (creating new accounts) and select I don't have a property or want to register it later ")
    public void TC001_Extranet_Signup_without_I_dont_have_account(Method method, String url, ITestContext ctx) throws Exception {
        startTest(method.getName()+" for "+url, "Signup from landing page (creating new accounts) and select I don't have a property or want to register it later ");
        ctx.getCurrentXmlTest().addParameter("title", "Signup from landing page and select I don't have a property for site >" + url);
        ArrayList<String> all_log = new ArrayList<String>();
        SignupPage sp = new SignupPage(driver);
        commonfunction cf = new commonfunction(driver);
        try {
            all_log.addAll(sp.Open_login_Page(url));
            all_log.addAll(sp.Click_SignUp_Page("cultbookingnotification+qatest" + commonfunction.emailnumber() + "@gmail.com", "eX3j6LAS@@86Vdh5"));
            all_log.addAll(sp.verifyEmail());
            all_log.addAll(sp.Your_details_Info());
            all_log.addAll(sp.property_details("WithoutProperty"));
        } catch (Throwable e) {
            ctx.getCurrentXmlTest().addParameter("description", cf.Steps_for_Gitlab_reporting(all_log));
            throw new Exception(e);
        }

    }
    @Test(priority = 1, alwaysRun = true, dataProvider = "SignupLink", description = "Signup from landing page (creating new accounts) and select I don't have a property or want to register it later ")
    public void TC002_Extranet_Signup_with_I_have_account(Method method, String url, ITestContext ctx) throws Exception {
        startTest(method.getName()+" for "+url, "Signup from landing page (creating new accounts) and select I  have a property ");
        ctx.getCurrentXmlTest().addParameter("title", "Signup from landing page (creating new accounts) and select I  have a property for site " + url);
        ArrayList<String> all_log = new ArrayList<String>();
        SignupPage sp = new SignupPage(driver);
        commonfunction cf = new commonfunction(driver);
        try {
            all_log.addAll(sp.Open_login_Page(url));
            all_log.addAll((sp.Click_SignUp_Page("cultbookingnotification+qatest" + commonfunction.emailnumber() + "@gmail.com", "eX3j6LAS@@86Vdh5")));
            all_log.addAll(sp.verifyEmail());
            all_log.addAll(sp.Your_details_Info());
            all_log.addAll((sp.property_details("withproperty")));
            all_log.addAll(sp.I_have_a_property_and_want_to_register_it_now());
        } catch (Throwable e) {
            ctx.getCurrentXmlTest().addParameter("description", cf.Steps_for_Gitlab_reporting(all_log));
            throw new Exception(e);
        }
    }

}