<?php
  
// ------------------------------------------------------------------------

/**
 * Plugin_model Class
 *
 * Handle all plugin database activity
 *
 * @package		Kalkun
 * @subpackage	Plugin
 * @category	Models
 */
class Plugin_model extends CI_Model {

	function get_plugins()
	{
		$this->db->from('plugins');
		$this->db->order_by('plugin_name', 'ASC');
		return $this->db->get();
	}
}
